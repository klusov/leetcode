﻿namespace  LeetCode;

public class LeetCode {
    public static void Main()
    {
        var lst = new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, null))));
        var mySolution = new Solution();
        mySolution.RemoveElements(lst, 3);
    }
}

public class Solution {
    public ListNode RemoveElements(ListNode head, int val) {

        var lst = new ListNode();
        var curr = lst;

        while(head != null)
        {
            if(head.val != val)
            {
                curr.next = head;
                curr = curr.next;
            }
            head = head.next;
        }

        curr.next = null;
        return lst.next;

    }
}



 public class ListNode {
     public int val;
     public ListNode next;
     public ListNode(int val=0, ListNode next=null) {
         this.val = val;
         this.next = next;
     }
 }
